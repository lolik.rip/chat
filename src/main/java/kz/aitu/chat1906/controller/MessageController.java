package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Message message) {
        messageService.add(message);
        return ResponseEntity.ok("Message successfully added");
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Message message) {
        messageService.update(message);
        return ResponseEntity.ok(message);
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Message message) {
        messageService.delete(message);
        return ResponseEntity.ok("Message successfully deleted");
    }

    @GetMapping("/messages/{chatId}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long chatId) {
        return ResponseEntity.ok(messageService.getMessagesByChat(chatId));
    }
}
