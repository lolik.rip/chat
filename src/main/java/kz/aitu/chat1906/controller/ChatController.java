package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chat")
public class ChatController {
    private final ChatService chatService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(chatService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Chat chat) {
        chatService.add(chat);
        return ResponseEntity.ok("Chat successfully added");
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Chat chat) {
        chatService.update(chat);
        return ResponseEntity.ok(chat);
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Chat chat) {
        chatService.delete(chat);
        return ResponseEntity.ok("Chat successfully deleted");
    }
}
