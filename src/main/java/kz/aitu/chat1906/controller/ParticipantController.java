package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/participant")
public class ParticipantController {
    private final ParticipantService participantService;

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Participant participant) {
        participantService.add(participant);
        return ResponseEntity.ok("Participant successfully added");
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Participant participant) {
        participantService.delete(participant);
        return ResponseEntity.ok("Participant successfully deleted");
    }

    @GetMapping("/chat/{chatId}")
    public ResponseEntity<?> getUsersByChatId(@PathVariable Long chatId){
        return ResponseEntity.ok(participantService.getUsersByChat(chatId));
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getChatsByUserId(@PathVariable Long userId){
        return ResponseEntity.ok(participantService.getChatsByUser(userId));
    }
}
