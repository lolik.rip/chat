package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    public List<Message> getMessagesByChat(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    public void add(Message message) {
        messageRepository.save(message);
    }

    public void update(Message message) {
        messageRepository.save(message);
    }

    public void delete(Message message) {
        messageRepository.delete(message);
    }
}
